## Tarea para PSP05.

### Ejercicio 1.

Modifica el ejemplo del servidor HTTP (Proyecto java ServerHTTP, apartado 5.1 de los contenidos) para que incluya la cabecera Date.

Una vez ya tenemos nuestra aplicación completada y la ejecutamos, ya es este momento en el que con nuestro navegador web accederemos 
a las distintas páginas alojadas en nuestro servidor HTTP, tal como se muestra en la siguiente imagen:

![Ejecutando la aplicación](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/Ejecutando%20la%20aplicacion.png)

Vamos ahora a acceder a alguna de las páginas que están alojadas en nuestro servidor HTTP, que en este caso vamos a poder acceder a través
de la siguiente URL:

http://localhost:8066

![Mostrando la página principal](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/Mostrando%20la%20p%C3%A1gina%20principal.PNG)

La siguiente página alojada en nuestro servidor HTTP muestra el comienzo de la obra El Quijote, cuyo autor es Miguel de Cervantes.

http://localhost:8066/quijote

![Mostrando el comienzo de El Quijote](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/El%20comienzo%20del%20Quijote.PNG)

Además de las páginas mostradas anteriormente, también disponemos de otra web que nos aparecerá en caso de que la dirección que introducimos
en el navegador cuando no existe el recurso.

http://localhost:8066/q

![Recurso no encontrado](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/Recurso%20no%20encontrado.PNG)

Vamos a comprobar el comportamiento de nuestro servidor HTTP desde la consola de nuestro Eclipse, tal como se muestra en la siguiente imagen:

![Comportamiento del servidor HTTP](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/Comprobando%20el%20comportamiento%20de%20nuestro%20servidor.jpg)
