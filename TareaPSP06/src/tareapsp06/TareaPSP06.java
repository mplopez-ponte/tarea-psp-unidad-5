/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareapsp06;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author Mar�a Paz L�pez Ponte
 */
public class TareaPSP06 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Variables
        Logger logger;
        FileHandler fh;
        SimpleFormatter formatter;
        Matcher mat1;
        Matcher mat2;
        Pattern pat1;
        Pattern pat2;
        String usuario;
        String archivo;
        String patron1;
        String patron2;
        File file=null;
        FileReader fr=null;
        BufferedReader br=null;
        boolean salida=false;
        
        // Establecemos los patrones a aplicar a las entradas de datos
        patron1="^[a-z]{8}$";
        patron2="^[a-zA-Z0-9]{1,8}\\.[a-zA-Z0-9]{3}$";
        pat1=Pattern.compile(patron1);
        pat2=Pattern.compile(patron2);
        
        // Creando el registro
        logger=Logger.getLogger("MyLog");
        
        // Mientras que la salida no sea true se repite la acci�n
       do{
           usuario=JOptionPane.showInputDialog("Nombre de usuario (8 letras min�sculas");
           try{
               // Manejamos el registro
               fh=new FileHandler("/src", true);
               formatter=new SimpleFormatter();
               fh.setFormatter(formatter);
               logger.addHandler(fh);
               logger.setLevel(Level.ALL);
               logger.log(Level.INFO, "Comprobando la validaci�n de datos...");
               
               // Se comprueba que lo introducido se corresponde a la plantilla
               mat1=pat1.matcher(usuario);
               if(mat1.find()){
            	   // Si la plantilla coincide con el patr�n
                   logger.log(Level.INFO, "Nombre de usuario aceptado");
                   do{
                	   // Se repite el nombre del archivo hasta que la salida sea true
                       archivo=JOptionPane.showInputDialog("Nombre del archivo al que quiere acceder "
                               + "(8 caracteres como m�ximo + extesi�n del archivo)");
                       
                       // Vamos a comprobar que lo introducido corresponde a la plantilla
                       mat2=pat2.matcher(archivo);
                       if(mat2.find()){
                    	   // Si se coincide con el patr�n
                           logger.log(Level.INFO, "Nombre de archivo aceptado");
                           file=new File(archivo);
                           if(file.exists()){
                        	   // Si el archivo existe lo mostramos por consola
                               logger.log(Level.FINE, "Localizado archivo requerido");
                               fr=new FileReader(file);
                               br=new BufferedReader(fr);
                               String linea;
                               while((linea=br.readLine())!=null){
                                   System.out.println(linea);
                               }
                               salida=true;
                               // Salimos del bucle
                           }else { 
                        	   // Si el archivo no existe mostramos un mensaje
                               JOptionPane.showMessageDialog(null,"El archivo no existe");
                               logger.log(Level.INFO, "Archivo no localizado");
                           }
                           
                       }else{
                    	   // Si el nombre del archivo no est� en el formato correcto mostramos el mensaje
                           JOptionPane.showMessageDialog(null, "Nombre de archivo en formato incorrecto");
                           logger.log(Level.INFO, "Formato de nombre de archivo incorrecto.");
                       }
                       
                   }while(salida==false);
                   
               }else {
            	   // Si el nombre de usuario no est� en el formato correcto mostramos el mensaje
                   JOptionPane.showMessageDialog(null, "Nombre de usuario en formato incorrecto");
               }
               
           }catch(IOException e){
        	   // Se capturan las excepciones de entrada o salida de datos
               System.err.println("Error de entrada y/o salida de datos");
               e.printStackTrace();
               logger.log(Level.WARNING, "Error de entrada o salida de datos");
           }catch(Exception ex){
        	   // Se capturan las excepciones en general que puedan darse
               System.err.print("Error");
               ex.printStackTrace();
               logger.log(Level.SEVERE, "Error sin definir");
           }finally{
        	   // Se cierra el flujo de datos en caso de que estuviese abierto
               if(br!=null){
                   try{
                    br.close();
                   }catch(IOException exz){
                       System.out.println("Error de entrada y/o salida");
                       exz.printStackTrace();
                       logger.log(Level.WARNING, "Error de entrada o salida de datos");
                   }
               }
           }
           
       } while(salida==false);
    }
}
