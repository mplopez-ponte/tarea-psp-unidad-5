## Tarea para PSP05.

### Ejercicio 2.

Modifica el ejemplo del servidor HTTP (Proyecto java ServerHTTP, apartado 5.1 de los contenidos) para que implemente multihilo, y pueda gestionar la concurrencia de manera eficiente.

Una vez ya tenemos nuestra aplicación finalizada y las pruebas son correctas, ejecutamos la aplicación tal como se muestra en la siguiente imagen:

![Ejecutando la aplicación](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/Ejecutando%20la%20aplicacion.png)

Al igual que en el primer ejercicio, en nuestro servidor HTTP disponemos de 3 páginas:

* Página príncipal

![Página principal](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/Mostrando%20la%20p%C3%A1gina%20principal.PNG)

* Página con el comienzo del Quijote

![Comienzo del Quijote](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/El%20comienzo%20del%20Quijote.PNG)

* Recurso no encontrado

![Recurso no encontrado](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/Recurso%20no%20encontrado.PNG)

Ahora vamos a implementar la concurrencia para que nuestro servidor HTTP pueda atender varias conexión generando varios hilos.

El código Java, para poder implementar esa concurrencia es el siguiente:

![Implementando concurrencia](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-5/downloads/Implementando%20concurrencia.PNG)
